**Steps to deploy locally:**

1. create a secret for postgres:<br>
    `kubectl create secret generic pgpassword --from-literal=PGPASSWORD=somepassword`
2. install kubernetes nginx ingress controller<br>
    `kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.1.1/deploy/static/provider/cloud/deploy.yaml`
3. Check if step 2 is complete<br>
    <code>kubectl wait --namespace ingress-nginx \\
        --for=condition=ready pod \\
        --selector=app.kubernetes.io/component=controller \\
        --timeout=120s</code>
4. apply k8s configuration<br>
    `kubectl apply -f k8s/`
5. check if all pods/deployments are fine<br>
    `kubectl get pods`<br>
    `kubectl get deployments`

Access the application at: http://localhost

